package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;



public class Fridge implements IFridge {

    private final int max_size = 20;
    private List<FridgeItem> items;

    public Fridge() {
        items = new ArrayList<FridgeItem>();
    }



    @Override
    public int nItemsInFridge() {
        return items.size();
    }



    @Override
    public int totalSize() {
        return max_size;
        
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot place 'null' in fridge");
        }
        if (items.size() >= max_size) {
            return false;
        }
        return items.add(item); //returnerer true
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot take out 'null' from fridge");
        }
        if (!items.contains(item)) {
            throw new NoSuchElementException("This item is not in the fridge");
        }
        items.remove(item);
    }


    @Override
    public void emptyFridge() {
        items.clear();

        
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        List <FridgeItem> expiredItems = new ArrayList<FridgeItem>();

        //Hjelpeliste
        for (FridgeItem item : items) { 
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }

        for (FridgeItem item : expiredItems) {
            if (item.hasExpired()) {
                items.remove(item);
            }
        }
        return expiredItems;
    }
}
